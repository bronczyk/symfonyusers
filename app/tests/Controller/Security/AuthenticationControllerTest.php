<?php

namespace App\Tests\Controller\Security;

use App\Tests\Common\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AuthenticationControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $this->client->request(
            Request::METHOD_GET,
            'http://localhost:8080/authentication/login',
        );

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Please log in');
    }
}