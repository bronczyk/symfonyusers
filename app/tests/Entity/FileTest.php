<?php

namespace App\Tests\Entity;

use App\Entity\File;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    const NAME = 'file_name';

    public function testFileCreate()
    {
        $file = new File();
        $this->assertInstanceOf(File::class, $file);

        $file->setName(self::NAME);
        $this->assertEquals(self::NAME, $file->getName());

        $now = new \DateTime('now');
        $file->setCreatedAt($now);

        $this->assertEquals($now, $file->getCreatedAt());
    }
}