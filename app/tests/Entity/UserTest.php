<?php


namespace App\Tests\Entity;


use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    const USER_NAME = 'name';
    const USER_LAST_NAME = 'last_name';
    const USER_BIRTHDAY = '1990-11-23 00:00:00';
    const USER_EMAIL = 'email@email.com';


    public function testUserCreate()
    {
        $user = new User();
        $this->assertInstanceOf(User::class, $user);

        $user->setName(self::USER_NAME);
        $user->setLastName(self::USER_LAST_NAME);

        $birthday = new \DateTime(self::USER_BIRTHDAY);

        $user->setBirthday($birthday);
        $user->setEmail(self::USER_EMAIL);
        $user->setUser(null);

        $now = new \DateTime('now');
        $user->setCreatedAt($now);

        $this->assertEquals(self::USER_NAME, $user->getName());
        $this->assertEquals(self::USER_LAST_NAME, $user->getLastName());
        $this->assertEquals(self::USER_EMAIL, $user->getEmail());
        $this->assertEquals($birthday, $user->getBirthday());
        $this->assertEquals($now, $user->getCreatedAt());
        $this->assertEquals(null, $user->getId());
        $this->assertEquals(null, $user->getUser());
    }
}