<?php

namespace App\Service;

use App\Entity\File;
use App\Entity\User;
use App\Message\UserPasswordEmail;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

class Import
{
    private EntityManagerInterface $entityManager;
    private $targetDirectory;
    protected $faker;

    private UserPasswordHasherInterface $hasher;

    private LoggerInterface $logger;

    private UserRepository $userRepository;

    private Security $security;

    private MessageBusInterface $bus;

    public function __construct($targetDirectory, EntityManagerInterface $entityManager, MessageBusInterface $bus,
                                UserPasswordHasherInterface $hasher, Security $security,
                                LoggerInterface $logger, UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->targetDirectory = $targetDirectory;
        $this->faker = Factory::create();
        $this->hasher = $hasher;
        $this->logger = $logger;
        $this->userRepository = $userRepository;
        $this->security = $security;
        $this->bus = $bus;
    }

    public function make(File $file)
    {
        $inputFileName = $this->getTargetDirectory() . '/' . $file->getName();

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $rows = $spreadsheet->getActiveSheet()->toArray();

        $this->createUsers($rows);
    }

    private function createUsers($rows)
    {
        $author = $this->security->getUser();

        foreach ($rows as $key => $row) {

            if ($key === 0) {
                continue;
            }

            $user = $this->userRepository->findOneBy(['email' => $row[3]]);

            if (!$user) {
                $user = new User();

                $user->setName($row[0]);
                $user->setLastName($row[1]);
                $user->setEmail($row[2]);
                $user->setBirthday((new \DateTime($row[3])));

                //$password = $this->faker->password(15, 15);
                $user->setPlainPassword($this->faker->password(15, 20));

                $password = $this->hasher->hashPassword($user, $user->getPlainPassword());
                $user->setPassword($password);

                $user->setUser($author);

                $this->entityManager->persist($user);

                $this->bus->dispatch(new UserPasswordEmail($user));
            }
        }

        $this->entityManager->flush();
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}