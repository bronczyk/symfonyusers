<?php

namespace App\Message;

use App\Entity\User;

class UserPasswordEmail
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}