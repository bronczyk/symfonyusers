<?php


namespace App\Message\MessageHandler;

use App\Message\UserPasswordEmail;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Address;

class UserPasswordHandler implements MessageHandlerInterface
{
    private MailerInterface $mailer;

    private LoggerInterface $logger;

    public function __construct(MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function __invoke(UserPasswordEmail $userPasswordEmail)
    {
        $email = (new TemplatedEmail())
            ->from('hello@example.com')
            ->to(new Address($userPasswordEmail->getUser()->getEmail()))
            ->subject('Thanks for signing up!')
            ->htmlTemplate('emails/new-password.html.twig')
            ->context([
                'user' => $userPasswordEmail->getUser(),
            ]);

        $this->mailer->send($email);

        $this->logger->info('Email message was sent to user ' . $userPasswordEmail->getUser()->getId());
    }
}