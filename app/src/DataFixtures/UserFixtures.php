<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;
    protected $faker;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
        $this->faker = Factory::create();
    }


    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('email@email.com');
        $user->setName($this->faker->name);
        $user->setLastName($this->faker->lastName);
        $user->setBirthday($this->faker->dateTimeBetween());

        $password = $this->hasher->hashPassword($user, 'password');
        $user->setPassword($password);
        $user->setUser(null);

        $manager->persist($user);
        $manager->flush();
    }
}