<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class CompareValues extends Constraint
{
    public $value1;
    public $value2;
    public $message = 'The values are the same.';

    public function __construct(
        $exactly = null,
        string $value1 = null,
        string $value2 = null,
        string $message = null,
        array $groups = null,
        $payload = null,
        array $options = []
    )
    {
        if (\is_array($exactly)) {
            $options = array_merge($exactly, $options);
            $exactly = $options['value'] ?? null;
        }

        $value1 = $value1 ?? $options['value1'] ?? null;
        $value2 = $value2 ?? $options['value2'] ?? null;

        unset($options['value1'], $options['value2']);

        parent::__construct($options, $groups, $payload);

        $this->value1 = $value1;
        $this->value2 = $value2;

        if (null === $this->value1 && null === $this->value2) {
            throw new MissingOptionsException(sprintf('Either option "value1" and "value2" must be given for constraint "%s".', __CLASS__), ['value1', 'value2']);
        }
    }

}

