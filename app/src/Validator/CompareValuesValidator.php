<?php


namespace App\Validator;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CompareValuesValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        $val1 = $this->context->getRoot()->get($constraint->value1)->getData();
        $val2 = $this->context->getRoot()->get($constraint->value2)->getData();

        if ($val1 === $val2) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}