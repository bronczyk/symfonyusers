<?php

namespace App\Validator;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ComparePasswordsValidator extends ConstraintValidator
{
    private UserPasswordHasherInterface $hasher;
    private Security $security;

    public function __construct(UserPasswordHasherInterface $hasher, Security $security)
    {
        $this->hasher = $hasher;
        $this->security = $security;
    }


    public function validate($value, Constraint $constraint)
    {
        $user = $this->security->getUser();

        if (!$this->hasher->isPasswordValid($user, $value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}