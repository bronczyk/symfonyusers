<?php


namespace App\Validator;

use Symfony\Component\Validator\Constraint;

class ComparePasswords extends Constraint
{
    public $message = 'Your current password is incorrect.';
}