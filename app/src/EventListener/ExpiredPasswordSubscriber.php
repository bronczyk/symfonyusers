<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;


class ExpiredPasswordSubscriber implements EventSubscriberInterface
{
    private Security $security;
    protected UrlGeneratorInterface $urlGenerator;

    public function __construct(Security $security, UrlGeneratorInterface $urlGenerator)
    {
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $user = $this->security->getUser();

        if ($user && $user->isPasswordExpired() && $event->getRequest()->getPathInfo() != '/security/password/renew') {
            $event->setResponse(new RedirectResponse($this->urlGenerator->generate('password_renew',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            )));
        }
    }
}