<?php

namespace App\Form\Type;

use App\Validator\ComparePasswords;
use App\Validator\CompareValues;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RenewPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', PasswordType::class, [
                'label_format' => 'Current password',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8, 'max' => 40]),
                    new ComparePasswords()
                ],
            ])
            ->add('new_password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options' => ['label' => 'New Password'],
                'second_options' => ['label' => 'Repeat New Password'],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8, 'max' => 40]),
                    new CompareValues([
                        'value1' => 'password',
                        'value2' => 'new_password',
                        'message' => 'The new password cannot be the same as the current password.'
                    ]),
                ],
            ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}