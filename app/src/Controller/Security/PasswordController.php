<?php

namespace App\Controller\Security;

use App\Form\Type\RenewPasswordType;
use App\Repository\UserRepository;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class AuthenticationController
 * @Route("/security/password", name="password_")
 */
class PasswordController extends AbstractController
{
    private UserPasswordHasherInterface $hasher;
    private Security $security;
    private UserRepository $userRepository;

    public function __construct(UserPasswordHasherInterface $hasher, Security $security, UserRepository $userRepository)
    {
        $this->hasher = $hasher;
        $this->security = $security;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/renew", methods={"GET","POST"}, name="renew")
     */
    public function renew(Request $request): Response
    {
        $form = $this->createForm(RenewPasswordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $user = $this->security->getUser();

            $this->userRepository->upgradePassword($user, $this->hasher->hashPassword($user, $data['new_password']));

            $this->addFlash('success', 'Your password has been changed!');

            return $this->redirectToRoute('user_index');
        }

        return $this->render('security/password/renew.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}