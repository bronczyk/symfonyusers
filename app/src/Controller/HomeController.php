<?php

namespace App\Controller;

use App\Repository\FileRepository;
use App\Service\Import;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthenticationController
 * @Route("/", name="home_")
 */
class HomeController extends AbstractController
{
    private FileRepository $fileRepository;
    /**
     * @var Import
     */
    private Import $import;

    public function __construct(FileRepository $fileRepository, Import $import)
    {
        $this->fileRepository = $fileRepository;
        $this->import = $import;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/", methods={"GET"}, name="index")
     */
    public function index(Request $request): Response
    {
        return $this->render('home/index.html.twig');
    }
}