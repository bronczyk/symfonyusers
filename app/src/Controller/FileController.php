<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\Type\UserFileType;
use App\Service\FileUploader;
use App\Service\Import;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class FileController
 * @Route("/file", name="file_")
 */
class FileController extends AbstractController
{
    private FileUploader $fileUploader;

    private EntityManagerInterface $entityManager;

    private Import $import;

    /**
     * FileController constructor.
     */
    public function __construct(FileUploader $fileUploader, EntityManagerInterface $entityManager, Import $import)
    {
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
        $this->import = $import;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/upload", methods={"GET","POST"}, name="upload")
     */
    public function upload(Request $request)
    {
        $model = new File();

        $form = $this->createForm(UserFileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();
            if ($file) {
                $fileName = $this->fileUploader->upload($file);
                $model->setName($fileName);

                $this->entityManager->persist($model);
                $this->entityManager->flush();

                $this->import->make($model);

                return $this->redirect($this->generateUrl('user_index'));
            }
        }

        return $this->render('file/upload.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}