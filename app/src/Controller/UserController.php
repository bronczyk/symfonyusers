<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class AuthenticationController
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    private UserRepository $userRepository;
    private PaginatorInterface $paginator;
    private Security $security;

    public function __construct(UserRepository $userRepository, PaginatorInterface $paginator, Security $security)
    {
        $this->userRepository = $userRepository;
        $this->paginator = $paginator;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/", methods={"GET"}, name="index")
     */
    public function index(Request $request)
    {
        $user = $this->security->getUser();

        $results = $this->userRepository->paginationList($user);

        $pagination = $this->paginator->paginate(
            $results, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            User::PAGE_LIMIT /*limit per page*/
        );

        return $this->render('user/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}