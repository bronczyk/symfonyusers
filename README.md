# README
## Jak uruchomić aplikację
1. `docker-compose up -d --build` w głównym katalogu aplikacji _(app)_
2. `docker exec -it php bash`
3. Przy pierwszym uruchomieniu 
    - `make init`
    - `php bin/console messenger:consume async` do uruchomienia workera odpowiedzialnego za obsługę kolejki do wysyłania wiadomości email
4. Uruchomienie testów — dodałem kilka prostych testów
    - Pierwsze uruchomienie `make tests`
    - Uruchomienie testów `symfony php bin/phpunit`
5. Przykładowy plik do importu użytkowników _users.xlsx_ znajduje się w głównym katalogu